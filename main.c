#include <stdio.h>
#include "mem.h"

int main() {
    puts("heap init test");
    struct mem * heap = heap_init(HEAP_PAGE_SIZE);
    mem_alloc_debug_heap(stdout, heap);

    puts("[mem_alloc] test 1");
    int * test1 = mem_alloc(sizeof(int) * 100);
    mem_alloc_debug_heap(stdout, heap);

    puts("[mem_alloc] test 2");
    int * test2 = mem_alloc(sizeof(int) * 200);
    mem_alloc_debug_heap(stdout, heap);

    puts("[mem_alloc] test 3");
    int * test3 = mem_alloc(sizeof(int) * 300);
    mem_alloc_debug_heap(stdout, heap);

    puts("[mem_alloc] test 4");
    int * test4 = mem_alloc(sizeof(int));
    mem_alloc_debug_heap(stdout, heap);

    puts("[mem_free] test 1");
    mem_free(test1);
    mem_alloc_debug_heap(stdout, heap);

    puts("[mem_free] test 2");
    mem_free(test2);
    mem_alloc_debug_heap(stdout, heap);

    puts("[mem_free] test 3");
    mem_free(test3);
    mem_alloc_debug_heap(stdout, heap);

    puts("[mem_free] test 4");
    mem_free(test4);
    mem_alloc_debug_heap(stdout, heap);

}
