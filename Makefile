all: main

main: main.c mem.c mem.h
	gcc main.c mem.c -o main

clean:
	rm -f main
