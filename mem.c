#include "mem.h"

static const int PROT_ALL = PROT_WRITE | PROT_READ;
static struct mem * heap = NULL;

static void * use_mmap(void * ptr, const size_t query, const bool is_fixed);
static bool block_splittable(struct mem * block, const size_t query);
static bool blocks_sequential(struct mem * prev, struct mem * next);
static void blocks_merge(struct mem * prev, struct mem * next);
static void block_split(struct mem * block, const size_t query);

void * heap_init(const size_t initial_size) {
	size_t real_size = initial_size;
    if (initial_size < HEAP_PAGE_SIZE) real_size = HEAP_PAGE_SIZE;
    heap = use_mmap(heap, real_size, false);
    heap->capacity = real_size - sizeof(struct mem);
    heap->is_free = true;
    heap->next = NULL;
    return heap;
}

void * mem_alloc(const size_t query) {
    struct mem * prev, * new_page;
    void * page_end;
    size_t page_size;
    size_t block_size = query;
    if (heap == NULL) {
        heap_init(HEAP_PAGE_SIZE);
    }

    if (query == 0) return NULL;
    if (query < BLOCK_MIN_SIZE) block_size = BLOCK_MIN_SIZE;

    for (struct mem * block = heap; block != NULL; block = block->next) {
        prev = block;
        if (!block->is_free) continue;
        if (block->capacity != block_size) {
            if (!block_splittable(block, block_size)) continue;
            block_split(block, block_size);
        }

        block->is_free = false;
        return (char *) block + sizeof(struct mem);
    }

    page_size = block_size + sizeof(struct mem);
    if (page_size < HEAP_PAGE_SIZE) page_size = HEAP_PAGE_SIZE;
    page_end = (char *) prev + sizeof(struct mem) + prev->capacity;


    new_page = use_mmap(page_end, page_size, true);
    if (new_page == MAP_FAILED) {

        new_page = use_mmap(page_end, page_size, false);
        if (new_page == MAP_FAILED) return NULL;
    }

    prev->next = new_page;
    new_page->capacity = page_size - sizeof(struct mem);
    new_page->is_free = false;
    new_page->next = NULL;

    if (block_splittable(new_page, block_size)) block_split(new_page, block_size);

    return (char*) new_page + sizeof(struct mem);

}

void mem_free(void * mem){
	struct mem * block = (struct mem *) ((char *) mem - sizeof(struct mem));
    struct mem * next, * prev;

    if (!mem) return;

    if (block != heap) {
        for (prev = heap; prev->next && prev->next != block; prev = prev->next);
        if (prev->next != block) return;
    }

    block->is_free = true;

    next = block->next;

    if (next && next->is_free && blocks_sequential(block, next)) blocks_merge(block, next);
    if (block != heap && prev->is_free && blocks_sequential(prev, block)) blocks_merge(prev, block);

}

void mem_alloc_debug_struct_info(FILE * f, struct mem const * const address) {
    char * ptr = (char *) address + sizeof(struct mem);

    fprintf(f, "start: %p, size: %4lu, is_free: %d, bytes:", (void *) address, address->capacity, address->is_free);
    for (size_t i = 0; i < DEBUG_FIRST_BYTES && i < address->capacity; i++, ptr++) {
        fprintf(f, " %2hhX", *ptr);
    }
    putc('\n', f);
}

void mem_alloc_debug_heap(FILE *f, struct mem const *ptr) {
    for (; ptr; ptr = ptr->next) mem_alloc_debug_struct_info(f, ptr);
}


static void * use_mmap(void * ptr, const size_t size, const bool is_fixed) {
	size_t real_size = size;
	if (size < HEAP_PAGE_SIZE) real_size = HEAP_PAGE_SIZE;
    return mmap(ptr, real_size, PROT_ALL, MAP_PRIVATE | MAP_ANONYMOUS | (is_fixed ? MAP_FIXED : 0), -1, 0);
}

static bool block_splittable(struct mem * block, const size_t query) {
    return block->capacity >= query + sizeof(struct mem) + BLOCK_MIN_SIZE;
}

static bool blocks_sequential(struct mem * prev, struct mem * next) {
    return (char *) next == (char *) prev + sizeof(struct mem) + prev->capacity;
}

static void blocks_merge(struct mem * prev, struct mem * next) {
    prev->capacity += sizeof(struct mem) + next->capacity;
    prev->next = next->next;
}

static void block_split(struct mem * block, const size_t query) {
    struct mem * new_block = (struct mem *) ((char *) block + sizeof(struct mem) + query);
    new_block->capacity = block->capacity - sizeof(struct mem) - query;
    new_block->is_free = true;
    new_block->next = NULL;
    block->capacity = query;
    block->next = new_block;
}
